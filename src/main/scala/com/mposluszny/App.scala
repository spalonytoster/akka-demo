import akka.actor._
import scala.collection.mutable.ListBuffer
import scala.util.Random
import scala.concurrent.duration._

object App {

  class TempSensorSupervisor extends Actor {
    private val temps = new ListBuffer[Double]
    private val sensors = List(
      context.actorOf(Props[TempSensor], "sensor1"),
		  context.actorOf(Props[TempSensor], "sensor2"),
		  context.actorOf(Props[TempSensor], "sensor3")  
    )
    
    def receive = {
  		case "start" => {
  		  println("Started sensors.")
//  		  for (sensor <- sensors) {
//  		    sensor ! "start"
//  		  }
  		  sensors.foreach(_ ! "start")
  		}
		  case "stop" => {
		    println("Stopped sensors.")
		    sensors.foreach(_ ! "stop")
		  }
			case temp: Double => temps += temp
			case "showAvg" => println("Avg temp: " + temps.sum / temps.size)
		}
  }
  
  class TempSensor extends Actor {
    import context.dispatcher
    
	  var measuring: Cancellable = null;
	  
		def receive = {
  		case "start" => {
  		  println("Started measuring temperature...")
//  		  measuring = context.system.scheduler
//  		    .schedule(0 millis, 2 seconds, context.parent, measure())
		    measuring = context.system.scheduler
		      .schedule(0 millis, 2 seconds) {
		        context.parent ! measure()
		      }
  		}
		  case "stop" => {
		    println("Stopped measuring temperature.")
		    if (!measuring.isCancelled) {
		      measuring.cancel()
		    }
		  }
			case "measure" => context.parent ! measure()
		}
		
		private def measure(): Double = {
		  val temp = (new Random().nextDouble() * 10) + 20
		  println(self.path.name + " measured " + temp + " degrees")
		  temp
		}
	}

	def main(args: Array[String]): Unit = {
		import system.dispatcher
	  
	  println("Hello Akka!")
		val system = ActorSystem("system")
		
		val supervisor = system.actorOf(Props[TempSensorSupervisor], "tempSupervisor")
		supervisor ! "start"
		
		system.scheduler
		  .schedule(1 second, 3 seconds, supervisor, "showAvg")
	}
}